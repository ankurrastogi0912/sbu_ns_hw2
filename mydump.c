#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/ethernet.h>
#include <time.h>

#define SNAP_LEN 1518
#define PROMISCUOUS_MODE 1 //PROMISCUOUS MODE
#define READ_TIME_OUT 1000 //Timeout in milliseconds
#define OPTIMIZE_FILTER 0
#define NUM_PACKETS -1
#define SIZE_ETHERNET 14
#define ETHER_ADDR_LENGTH 6

#define IP_HL(ip)               (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)                (((ip)->ip_vhl) >> 4)

/* Ethernet header */
struct sniff_ethernet {
    u_char  ether_dhost[ETHER_ADDR_LENGTH];    /* destination host address */
    u_char  ether_shost[ETHER_ADDR_LENGTH];    /* source host address */
    u_short ether_type;                     /* IP? ARP? RARP? etc */
};

/* IP header */
struct sniff_ip {
    u_char  ip_vhl;                 /* version << 4 | header length >> 2 */
    u_char  ip_tos;                 /* type of service */
    u_short ip_len;                 /* total length */
    u_short ip_id;                  /* identification */
    u_short ip_off;                 /* fragment offset field */
    #define IP_RF 0x8000            /* reserved fragment flag */
    #define IP_DF 0x4000            /* dont fragment flag */
    #define IP_MF 0x2000            /* more fragments flag */
    #define IP_OFFMASK 0x1fff       /* mask for fragmenting bits */
    u_char  ip_ttl;                 /* time to live */
    u_char  ip_p;                   /* protocol */
    u_short ip_sum;                 /* checksum */
    struct  in_addr ip_src,ip_dst;  /* source and dest address */
};

typedef u_int tcp_seq;

struct sniff_tcp {
    u_short th_sport;               /* source port */
    u_short th_dport;               /* destination port */
    tcp_seq th_seq;                 /* sequence number */
    tcp_seq th_ack;                 /* acknowledgement number */
    u_char  th_offx2;               /* data offset, rsvd */
    #define TH_OFF(th)      (((th)->th_offx2 & 0xf0) >> 4)
    u_char  th_flags;
    #define TH_FIN  0x01
    #define TH_SYN  0x02
    #define TH_RST  0x04
    #define TH_PUSH 0x08
    #define TH_ACK  0x10
    #define TH_URG  0x20
    #define TH_ECE  0x40
    #define TH_CWR  0x80
    #define TH_FLAGS        (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)
    u_short th_win;                 /* window */
    u_short th_sum;                 /* checksum */
    u_short th_urp;                 /* urgent pointer */
};

char *interface, *file, *filter_string, *expression;


//Reference - Method taken from http://www.tcpdump.org/sniffex.c
void print_hex_ascii_line(const u_char *payload, int len, int offset)
{

    int i;
    int gap;
    const u_char *ch;

    /* offset */
    printf("%05d   ", offset);
    
    /* hex */
    ch = payload;
    for(i = 0; i < len; i++) {
        printf("%02x ", *ch);
        ch++;
        /* print extra space after 8th byte for visual aid */
        if (i == 7)
            printf(" ");
    }
    /* print space to handle line less than 8 bytes */
    if (len < 8)
        printf(" ");
    
    /* fill hex gap with spaces if not full line */
    if (len < 16) {
        gap = 16 - len;
        for (i = 0; i < gap; i++) {
            printf("   ");
        }
    }
    printf("   ");
    
    /* ascii (if printable) */
    ch = payload;
    for(i = 0; i < len; i++) {
        if (isprint(*ch))
            printf("%c", *ch);
        else
            printf(".");
        ch++;
    }

    printf("\n");

    return;
}

//Reference - Method taken from http://www.tcpdump.org/sniffex.c
void print_payload(const u_char *payload, int len)
{

    int len_rem = len;
    int line_width = 16;            /* number of bytes per line */
    int line_len;
    int offset = 0;                 /* zero-based offset counter */
    const u_char *ch = payload;

    if (len <= 0)
        return;

    /* data fits on one line */
    if (len <= line_width) {
        print_hex_ascii_line(ch, len, offset);
        return;
    }

    /* data spans multiple lines */
    for ( ;; ) {
        /* compute current line length */
        line_len = line_width % len_rem;
        /* print line */
        print_hex_ascii_line(ch, line_len, offset);
        /* compute total remaining */
        len_rem = len_rem - line_len;
        /* shift pointer to remaining bytes to print */
        ch = ch + line_len;
        /* add offset */
        offset = offset + line_width;
        /* check if we have line width chars or less */
        if (len_rem <= line_width) {
            /* print last line and get out */
            print_hex_ascii_line(ch, len_rem, offset);
            break;
        }
    }

    return;
}

int findStr(char *str, char *substr, int payloadSize) 
{
    int i = 0;
    while (i < payloadSize){

        int stringMatched = 0;

        if(str[i] == substr[0]){
            int j = 0;
            stringMatched = 1;
            while(substr[j] != '\0'){
                if(str[i + j] != substr[j]){
                    stringMatched = 0;
                    break;
                }
                j++;
            }
        }

        if(stringMatched == 1){
            return 1;
        }

        i++;
    }

    return 0;
}


void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet){

    const struct sniff_ethernet *ethernet;
    const struct sniff_ip *ip;
    const struct sniff_tcp *tcp;
    const u_char *payload;
    int string_matching_required = 0;
    int string_matched = 0;

    int size_ip;
    int size_tcp;
    int size_payload;
 
    char *protocol;

    ethernet = (struct sniff_ethernet*)(packet);
    ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
    size_ip = IP_HL(ip)*4;
    if (size_ip < 20){
   	    return;
    }

    /* determine protocol */    
    switch(ip->ip_p) {
        case IPPROTO_TCP:
	       protocol = "TCP";
            break;
        case IPPROTO_UDP:
	       protocol = "UDP";
            break;
        case IPPROTO_ICMP:
	       protocol = "ICMP";
            break;
        case IPPROTO_IP:
	       protocol = "IP";
            break;
        default:
	       protocol = "unknown";
            break;
    }

    tcp = (struct sniff_tcp*)(packet + SIZE_ETHERNET + size_ip);
    size_tcp = TH_OFF(tcp)*4;

    if(size_tcp < 20){
    }

    char tmbuf[64], buf[64];

    strftime(tmbuf, sizeof tmbuf, "%Y-%m-%d %H:%M:%S", localtime(&header->ts.tv_sec));
    snprintf(buf, sizeof buf, "%s.%06ld", tmbuf, (long)header->ts.tv_usec);

    payload = (u_char *)(packet + SIZE_ETHERNET + size_ip + size_tcp);
    size_payload = ntohs(ip->ip_len) - (size_ip + size_tcp);

    if(filter_string){
        string_matching_required = 1;
    }

    if(string_matching_required && findStr((char *)payload, filter_string, size_payload) != 0 && size_payload > 0){
        string_matched = 1;
    }

    if(string_matching_required == 1 && string_matched == 0){
        return;
    }

    printf("%s ", buf);
    printf("%02x:%02x:%02x:%02x:%02x:%02x ", 
        ethernet->ether_shost[0], ethernet->ether_shost[1],
        ethernet->ether_shost[2], ethernet->ether_shost[3],
        ethernet->ether_shost[4], ethernet->ether_shost[5]);
    printf("-> ");
    printf("%02x:%02x:%02x:%02x:%02x:%02x ", 
        ethernet->ether_dhost[0], ethernet->ether_dhost[1],
        ethernet->ether_dhost[2], ethernet->ether_dhost[3],
        ethernet->ether_dhost[4], ethernet->ether_dhost[5]);
    printf("type %#x ", ethernet->ether_type);
    printf("len %d ", header->len);
    printf("%s:%d ", inet_ntoa(ip->ip_src), ntohs(tcp->th_sport));
    printf("-> ");
    printf("%s:%d ", inet_ntoa(ip->ip_dst), ntohs(tcp->th_dport));
    printf("%s ", protocol);
    printf("dlen %d\n", size_payload);
    
    if(size_payload > 0){
        print_payload(payload, size_payload);
    }
   
}


int main(int argc, char* argv[]){


    for (int i = 1; i < argc; i++){
        if(strcmp(argv[i],"-i") == 0){
            i++;
            interface = argv[i];
        } else if(strcmp(argv[i],"-r") == 0){
            i++;
            file = argv[i];
        } else if(strcmp(argv[i],"-s") == 0){
            i++;
            filter_string = argv[i];
        } else {
            expression = argv[i];
            for (int i = 0; expression[i]; i++){
                expression[i] = tolower(expression[i]);
            }
        }    
    }

    
    char* dev, errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle;
    struct bpf_program fp;
    char *filter_exp = (char *) malloc(2048);
    bpf_u_int32 net;
    bpf_u_int32 mask;
    const u_char *packet;
    struct pcap_pkthdr header;

    //if device is passed as input argument
    if(interface) {
	   dev = interface;
    } else {
    	dev = pcap_lookupdev(errbuf);
    }

    //if filter_exp is passed as argument variable
    if(expression){
       filter_exp = expression;
    } else {
    }

    if(dev == NULL) {
    	fprintf(stderr, "Couldn't find default device: %s\n", errbuf);
	return(2);
    }

    if(pcap_lookupnet(dev, &net, &mask, errbuf) == -1){
	fprintf(stderr, "Not able to get net mask for device %s \n", dev);
 	net = 0;
	mask = 0;
    }

    if(file){
        handle = pcap_open_offline(file,errbuf);
    } else {
        handle = pcap_open_live(dev, SNAP_LEN, PROMISCUOUS_MODE, READ_TIME_OUT, errbuf);
    }

    if(handle == NULL){
        fprintf(stderr, "Could not open device %s: %s\n", dev, errbuf);
	    return(2);
    }

    if(pcap_datalink(handle) != DLT_EN10MB){
	   fprintf(stderr, "Device %s doesn't support ethernet\n", dev);
	   return(2);
    }

    if(pcap_compile(handle, &fp, filter_exp, OPTIMIZE_FILTER, net) == -1){
	   fprintf(stderr, "Couldn't parse filter: %s: %s \n", filter_exp, pcap_geterr(handle));
	   return(2);
    }

    if(pcap_setfilter(handle, &fp) == -1){
	   fprintf(stderr, "Not able to install filter %s: %s\n", filter_exp, pcap_geterr(handle));
	   return(2);
    } 

    pcap_loop(handle, NUM_PACKETS, got_packet, NULL);

    pcap_freecode(&fp);
    pcap_close(handle);

    return(0);
}
